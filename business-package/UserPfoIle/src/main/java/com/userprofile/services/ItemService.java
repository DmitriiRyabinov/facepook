package com.userprofile.services;

import com.userprofile.models.entity.Item;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface ItemService {

    List<Item> getAllItems();

    Optional<Item> getItemById(Long id);
}
