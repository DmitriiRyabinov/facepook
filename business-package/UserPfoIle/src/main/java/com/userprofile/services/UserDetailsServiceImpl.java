package com.userprofile.services;

import com.userprofile.models.entity.User;
import com.userprofile.models.entity.security.UserDetailsImpl;
import com.userprofile.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user.getEmail().isEmpty()) {
                 throw new UsernameNotFoundException(String.format("Пользователь с ткаим Email не найден", email));
        }
        return UserDetailsImpl.create(user);

    }

}
