package com.userprofile.services;

import com.userprofile.models.entity.Item;
import com.userprofile.models.entity.User;
import com.userprofile.models.entity.UserAccountRole;
import com.userprofile.repository.ItemRepository;
import com.userprofile.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ItemRepository itemRepository;

    public UserServiceImpl(UserRepository userRepository, ItemRepository itemRepository) {
        this.userRepository = userRepository;
        this.itemRepository = itemRepository;
    }

    @Override
    public User createUser(User user) {
        user.setRole(UserAccountRole.ROLE_USER);

        return userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllFriends(String name) {
        Optional<User> o = Optional.ofNullable(userRepository.findByEmail(name));
        List<User> u = o.get().getFriends().stream()
                .map(userRepository::getById)
                .collect(Collectors.toList());
        return u;
    }

    @Override
    public User addFriend(User user1, Principal p) {
        User u1 = findByEmail(p.getName());
        List<Long> a = new ArrayList<>(u1.getFriends());
        a.add(user1.getFriends().get(0));
        u1.setFriends(a);
        userRepository.save(u1);
        return u1;
    }

    @Override
    public User deleteFriend(User user1, Principal p) {
        User u1 = findByEmail(p.getName());
        List<Long> a = new ArrayList<>(u1.getFriends());
        a.remove(user1.getFriends().get(0));
        u1.setFriends(a);
        userRepository.delete(u1);
        return u1;
    }

    @Override
    public User addItem(Long id) {
        User u = new User();
        Set<Item> i = new HashSet<>();
        i.add(itemRepository.getById(id));
        u.setItems(i);
        userRepository.save(u);
        return u;
    }
   }
