package com.userprofile.services;

import com.userprofile.models.entity.User;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

public interface UserService {

    User createUser(User user);

    User findByEmail(String email);

    List<User> getAllUsers();

    List<User> getAllFriends(String name);

    User addFriend(User user1, Principal p);

    User deleteFriend(User user1, Principal p);

    User addItem(Long id);

}
