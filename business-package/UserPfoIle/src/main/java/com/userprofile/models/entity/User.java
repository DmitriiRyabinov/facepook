package com.userprofile.models.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "users")
public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String password;
    private String name;
    private String surname;
    private Double money;
    @Enumerated(EnumType.STRING)
    private UserAccountRole role;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Item> items;

    @ElementCollection
    @CollectionTable(
            name = "users_friends",
            joinColumns = @JoinColumn(name = "user_id"))
    private List<Long> friends;

    public User() {
    }

    public User(String email,
                String password, UserAccountRole role,
                String name, String surname, Double money) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.name = name;
        this.surname = surname;
        this.money = money;
    }

    public User(String email,
                String password, UserAccountRole role,
                String name, String surname, Double money, List<Long> friends) {
        this(email, password, role, name, surname, money);
        this.friends = friends;
    }
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public User(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserAccountRole getRole() {
        return role;
    }

    public void setRole(UserAccountRole role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Long> getFriends() {

        return friends;
    }

    public void setFriends(List<Long> friends) {

        this.friends = friends;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", friends=" + friends +
                '}';
    }
}
