package com.userprofile.models.entity;

import org.springframework.security.core.GrantedAuthority;

public enum UserAccountRole implements GrantedAuthority {
    ROLE_USER("ROLE_USER");

    private String role;

    UserAccountRole(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }
}
