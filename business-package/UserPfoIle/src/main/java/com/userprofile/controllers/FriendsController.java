package com.userprofile.controllers;

import com.userprofile.models.entity.User;
import com.userprofile.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;


@Controller
@RequestMapping("/main/friends")
public class FriendsController {

    private final UserService userService;

    public FriendsController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping
    public String pageFriends() {
        return "friends";
    }

    @ResponseBody
    @GetMapping("/getMyFriends")
    public ResponseEntity<Iterable<User>> getMyFriends(Principal p) {
        return ResponseEntity.ok().body(userService.getAllFriends(p.getName()));
    }

    @ResponseBody
    @PostMapping("/addFriend")
    public ResponseEntity<User> addMyFriends(@ModelAttribute("user") User user1, Principal p) {
        return ResponseEntity.ok(userService.addFriend(user1, p));
    }

    @GetMapping("/addFriend")
    public String addPage() {
        return "addFriend";
    }
    @ResponseBody
    @PostMapping("/deleteFriend")
    public ResponseEntity<User> deleteFriends(@ModelAttribute("user") User user1, Principal p) {
        return ResponseEntity.ok(userService.deleteFriend(user1, p));
    }

    @GetMapping("/deleteFriend")
    public String deletePage() {
        return "deleteFriend";
    }
}
