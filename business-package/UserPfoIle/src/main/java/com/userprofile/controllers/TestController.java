package com.userprofile.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping
    @ApiOperation("Тестовый метод")
    public String testPage() {
        return "Hello test";
    }
    //http://localhost:8079/swagger-ui/
}
