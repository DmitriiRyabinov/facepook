package com.userprofile.controllers;

import com.userprofile.models.entity.User;
import com.userprofile.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String LoginPage() {
        return "login";
    }

    @GetMapping("/reg")
    public String registerPage() {
        return "reg";
    }

    @PostMapping("/reg")
    public String addUser(@ModelAttribute("user") User user) {
        userService.createUser(user);
        return "redirect:/login";
    }
}
