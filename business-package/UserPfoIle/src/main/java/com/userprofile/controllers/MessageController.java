package com.userprofile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/main/message")
public class MessageController {

    @GetMapping
    public String pageMessage() {
        return "message";
    }

//    @ResponseBody
//    @GetMapping("/getAllMessage")
//    public ResponseEntity<Iterable<Friend>> getAllMessage() {
//        return ResponseEntity.ok().body(friendService.getAllFriends());
//    }
//
//    @ResponseBody
//    @PostMapping("/addMessage")
//    public ResponseEntity<Friend> addMyFriends(@RequestBody Friend friend) {
//        return ResponseEntity.ok().body(friendService.addFriend(friend));
//    }
//    @ResponseBody
//    @DeleteMapping("/deleteMessage")
//    public ResponseEntity<Void> deleteFriends(@RequestBody Friend friend) {
//        return ResponseEntity.ok().body(friendService.delete(friend));
//    }
}
