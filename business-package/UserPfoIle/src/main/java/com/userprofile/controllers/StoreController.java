package com.userprofile.controllers;

import com.userprofile.models.entity.Item;
import com.userprofile.services.ItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/main/store")
public class StoreController {

    private final ItemService itemService;

    public StoreController(ItemService itemService) {
        this.itemService = itemService;
    }

    @ResponseBody
    @GetMapping("/getAllItems")
    public ResponseEntity<Iterable<Item>> getItems() {
        return ResponseEntity.ok().body(itemService.getAllItems());
    }

    @ResponseBody
    @GetMapping("/getItem/{id}")
    public ResponseEntity<Optional<Item>> getItemById(@PathVariable Long id) {
        return ResponseEntity.ok(itemService.getItemById(id));
    }

}
