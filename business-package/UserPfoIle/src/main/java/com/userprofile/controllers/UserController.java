package com.userprofile.controllers;

import com.userprofile.models.entity.User;
import com.userprofile.services.ItemService;
import com.userprofile.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/main")
public class UserController {

    private final UserService userService;
    private final ItemService itemService;

    public UserController(UserService userService, ItemService itemService) {
        this.userService = userService;
        this.itemService = itemService;
    }

    @ResponseBody
    @GetMapping("/friends/getAllUsers")
    public ResponseEntity<Iterable<User>> getAllUsers() {
        return ResponseEntity.ok().body(userService.getAllUsers());
    }

    @ResponseBody
    @PostMapping("/store/addItem/{id}")
    public ResponseEntity<User> addItem(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.addItem(id));
    }

}
