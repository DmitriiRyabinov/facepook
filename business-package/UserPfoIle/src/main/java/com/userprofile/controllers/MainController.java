package com.userprofile.controllers;

import com.userprofile.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/main")
public class MainController {

    private final UserService userService;

    public MainController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String pageMain(Principal principal, Model m) {
        m.addAttribute("user", principal.getName());
        return "main";
    }

    @GetMapping("/news")
    public String pageNews() {
        return "news";
    }



    @GetMapping("/photos")
    public String pagePhotos() {
        return "photos";
    }
}
